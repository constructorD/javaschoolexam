package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

            String statementx = statement;

            //  System.out.println(checkError(statement));

       //     System.out.println(statementx);

            if (checkError(statementx)) {
                //System.out.println("error");
                return null;
            }
            else {
                // System.out.println(checkBr(statement));

                while (checkBr(statement)){
       //             System.out.println(checkBr(statement));
                    statementx = diffCount(statementx);
                }

                Double xz = count(statementx);
                int xy = Integer.parseInt("" + xz);
           //     System.out.println(xy);

                //System.out.println(diffCount(statement));
                //System.out.println(count(statement));

                Double d = xz;
                d = d * 1000;
                int i = (int) Math.round(d);
                d = (double)i / 1000;

                return "" + d;
            }

           // return null;
    }



        public static boolean checkBr(String statement){

            char[] array = statement.toCharArray();

            for (int i = 0; i < array.length; i++){
                if (array[i] == ')' || array[i] == '('){return true;}
            }

            return false;
        }

        public static String diffCount (String statement) {

            char[] array = statement.toCharArray();

            int start = 0 , end = 0;

            for (int i = 0; i < array.length; i++){
                if (array[i] == ')'){end = i; break;}
            }
            for (int i = end - 1; i > 0; i-- ){
                if (array[i] == '('){start = i; break;}
            }

            String temp = "";
            for (int i = start + 1; i < end ; i++){
                temp += array[i];
            }

            //     System.out.println(start + " " + end);
            //     System.out.println(temp);

            Double x = count(temp);
            if (x == null){return null;}


            //     System.out.println(x);

            String temp2 = "";
            for (int i = 0; i < start; i++){
                temp2 += array[i];
            }
            //  if (x < 0) {temp2 += "0" + x;}
            // else
            {temp2 += x;}
            for (int i = end + 1; i < array.length; i++) {
                temp2 += array[i];
            }

     //       System.out.println(temp2);

            return temp2;
        }

        public static boolean checkError(String statement) {

            if (statement == "" || statement == null) {return true;}

            statement += '!';
            char[] array = statement.toCharArray();
            boolean error = false;

            char first = ' ';
            char second = ' ';

            int countBr = 0;

            for (int i = 0; i < array.length - 1; i++) {
                first = array[i];
                second = array[i + 1];
                if (first == ','){error = true;}
                if (first == ' '){error = true;}
                if (first == '.' && second == '.'){error = true;}
                if (first == '/' && second == '/'){error = true;}
                if (first == '*' && second == '*'){error = true;}
                if (first == '+' && second == '+'){error = true;}
                if (first == '-' && second == '-'){error = true;}
                if (first == ')') {countBr++;}
                if (first == '(') {countBr--;}
                if (first == '(' && second == ')'){error = true;}
                if (first == ')' && second == '('){error = true;}
            }
            if (countBr != 0){error = true;}

            return error;
        }


        public static Double count (String statement) {

            Stack<Double> numbers = new Stack<>();
            Stack<Character> symbols = new Stack<>();

            statement += "!";
            char[] array = statement.toCharArray();

            String temp = "";

            for (int i = 0; i <array.length; i++) {
                char mem = array[i];
                if (Character.isDigit(mem)) {
                    temp += mem;
                }
                else {
                    if (mem == '.'){temp += mem;continue;}
                    if (temp == "")
                    {temp = "-"; continue;}
                    numbers.push(Double.parseDouble(temp));
                    temp = "";
                    //        if (mem == '-'){temp += "-";continue;}
                    if (symbols.isEmpty()){
                        symbols.push(mem);
                    }
                    else {
                        if (mem == '!'){continue;}

                        int memZ = 1;
                        int z = 1;
                        char t = symbols.lastElement();
                        if ((mem == '*') || (mem == '/')) {memZ = 2;}
                        if ((t == '*') || (t == '/')) {z = 2;}

                        if (memZ <= z) {
                  /*      if (mem == '-' && !Character.isDigit(t))
                        {
                            array[i + 1] *= -1;
                        numbers.push(numbers.pop() * -1);
                        }*/

                            numbers.push(easyCount(numbers.pop(),numbers.pop(),symbols.pop()));
                            //            System.out.println(numbers.lastElement());
                            symbols.push(mem);
                        }
                        else {
                            symbols.push(mem);
                        }

                    }
                }
            }

            while (!symbols.isEmpty()) {

                char I = symbols.pop();
                if (!symbols.isEmpty() && symbols.lastElement() == '-') {
                    symbols.pop();
                    symbols.push('+');
                    numbers.push(easyCount(numbers.pop(), numbers.pop() * (-1), I));
                } else {
                    numbers.push(easyCount(numbers.pop(), numbers.pop(), I));
                }
            }

            //   System.out.println(numbers);
            //   System.out.println(symbols);

            return numbers.pop();
        }


        public static Double easyCount(Double x, Double y, char I) {
            if (x == null) {return null;}
            if (y == null) {return null;}
            switch (I) {

                case '+':
                    return (x + y);
                case '-':
                    return (y - x);
                case '*':
                    return (x * y);
                case '/':
                    if (x == 0) {return null;}
                    return (y / x);
                default:return 0.0;
            }
        }
    }




