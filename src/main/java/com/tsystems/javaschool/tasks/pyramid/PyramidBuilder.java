package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

//        List<Integer> input2 = new ArrayList<>();


        List<Integer> input = inputNumbers;

        boolean check = false;

        int x = 3;
        int temp = 0;
        //     System.out.println(temp);
        if (input.size() < x){throw new CannotBuildPyramidException();}
        if(input.size() > 100){throw new CannotBuildPyramidException();}
        if (input.size() == x){check = true;} else {

            for (int i = 3; true; i++) {
                x += i;
                //   System.out.println(x + "  i : " + i);
                //    if (i == 0){x -= 1;}
                //     if (i == 1){x -= 1;};

                if (input.size() < x) {
                    throw new CannotBuildPyramidException();
                }
                if (input.size() == x) {
                    check = true;
                    temp = i - 3;
                    //         System.out.println("temp " + temp);
                    break;
                }
            }
        }
        if (check) {

            for (int i = 0; i < input.size(); i++){
                if (input.get(i) == null){throw new CannotBuildPyramidException();}
            }
            //       System.out.println(input);
            input = sort(input);
            //      System.out.println(input);

            return buildP(input, temp);

            // System.out.println("hello uno");
        }

        if (!check){
            throw new CannotBuildPyramidException();
            //System.out.println("oops");
        }

        return new int[0][0];
    }
    public static List<Integer> sort (List<Integer> list) {

        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i) > list.get(j)){
                    int temp = list.get(i);
                    list.set(i, list.get(j));
                    list.set(j, temp);
                }
            }
        }

        return list;
    }

    public static int[][] buildP (List<Integer> list, int Q) {

        //  System.out.println(Q);
        if (list.size() > 3){Q++;}
        int hor = 3 + 2*Q;
        int ver = Q + 2;
        //    System.out.println(Q);

        int[][] pyramid = new int[hor][ver];

        for (int i = 0; i < ver; i++ ){
            for (int j = 0; j < hor; j++) {
                pyramid[j][i] = 0;
                //      System.out.print(pyramid[j][i]);
            }
            //    System.out.println();
        }
        int count = 0;
        for (int i = 0; i < ver; i++) {
            if ((i % 2) == 0){
                //    System.out.print(" chet ");

                //     System.out.println(count);
                int middle = ver - 1;
                int start = middle - i ;


                for (int j = i + 1; j > 0; j--){

                    pyramid[start][i] = list.get(count);
                    start += 2;
                    count++;
                }
            }
            else {
                //       System.out.print(" chet ");
                //     System.out.println(count);
                int middle = ver - 1;
                int start = middle - i ;


                for (int j = i + 1; j > 0; j--){

                    pyramid[start][i] = list.get(count);
                    start += 2;
                    count++;
                }

            }

        }

        //      System.out.println();
        for (int i = 0; i < ver; i++ ){
            for (int j = 0; j < hor; j++) {
         //       System.out.print(pyramid[j][i]);
            }
          //  System.out.println();
        }
        // System.out.println(pyramid);
        int[][] testP = new int[ver][hor];
        for (int i = 0; i < ver; i++ ){
            for (int j = 0; j < hor; j++) {
                testP[i][j] = pyramid[j][i];
          //      System.out.print(pyramid[j][i]);
            }
        //    System.out.println();
        }

        return testP;
    }

}
