package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws  IllegalArgumentException{
        // TODO: Implement the logic here


        boolean check = true;

        if(x == null || y == null){throw new IllegalArgumentException();}

     //   System.out.println(x);
     //   System.out.println(y);

        int scan = 0;
        if(x.size() == 0 || y.size() == 0)
        {
            //return false
            //  System.out.println("false");
        }

        int i = 0;
        int j = 0;

        while (check) {
            if (x.size() == 0){check = false; break;}
            if (j >= y.size()){check = false; break;}

            if (x.get(i) == y.get(j)){
                x.remove(i);
                y.remove(j);

            }
            else{
                j++;

            }
            if (j > y.size())check = false;
        }

        //return check;

      //  System.out.println(x);
       // System.out.println(y);

        if (x.size() == 0) {
          //  System.out.println("true");
            return true;
        }
        else
        {
         //   System.out.println("false");
            return false;
        }
    }
}
